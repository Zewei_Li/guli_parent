package com.example.cms.mapper;

import com.example.cms.entity.CrmBanner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 首页banner表 Mapper 接口
 * </p>
 *
 * @author 李泽伟
 * @since 2022-05-31
 */
public interface CrmBannerMapper extends BaseMapper<CrmBanner> {

}
