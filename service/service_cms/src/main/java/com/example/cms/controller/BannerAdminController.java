package com.example.cms.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.cms.entity.CrmBanner;
import com.example.cms.service.CrmBannerService;
import com.example.utils.CommonResult;
import org.apache.ibatis.annotations.Delete;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 首页banner表 前端控制器
 * </p>
 *
 * @author 李泽伟
 * @since 2022-05-31
 */
@RestController
@RequestMapping("/educms/banneradmin")
//@CrossOrigin
public class BannerAdminController {
    @Resource
    CrmBannerService crmBannerService;

    //1 分页查询banner
    @GetMapping(value = "pageBanner/{page}/{limit}")
    public CommonResult pageBanner(@PathVariable Long page,@PathVariable Long limit){
        Page<CrmBanner> crmBannerPage=new Page<>(page,limit);
        crmBannerService.page(crmBannerPage,null);
        return CommonResult.ok().data("items",crmBannerPage.getRecords()).data("total",crmBannerPage.getTotal());
    }
    //2 添加banner
    @PostMapping(value = "addBanner")
    public CommonResult addBanner(@RequestBody CrmBanner crmbanner){
        crmBannerService.save(crmbanner);
        return CommonResult.ok();
    }
    //3 通过id获取banner
    @GetMapping(value = "get/{id}")
    public CommonResult get(@PathVariable String id){
        CrmBanner byId = crmBannerService.getById(id);
        return CommonResult.ok().data("item",byId);
    }
    //4 修改banner
    @PutMapping(value = "update")
    public CommonResult update(@RequestBody CrmBanner crmBanner){
        crmBannerService.updateById(crmBanner);
        return CommonResult.ok();
    }
    //5 通过id删除banner
    @DeleteMapping(value = "remove/{id}")
    public CommonResult remove(@PathVariable String id){
        crmBannerService.removeById(id);
        return CommonResult.ok();
    }

}

