package com.example.cms.controller;


import com.example.cms.entity.CrmBanner;
import com.example.cms.service.CrmBannerService;
import com.example.utils.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 首页banner表 前端控制器
 * </p>
 *
 * @author 李泽伟
 * @since 2022-05-31
 */
@RestController
@RequestMapping("/educms/bannerfront")
//@CrossOrigin
public class BannerFrontController {

    @Autowired
    private CrmBannerService bannerService;

    //查询所有banner
    @GetMapping("getAllBanner")
    public CommonResult getAllBanner() {
        List<CrmBanner> list = bannerService.selectAllBanner();
        return CommonResult.ok().data("list",list);
    }
}

