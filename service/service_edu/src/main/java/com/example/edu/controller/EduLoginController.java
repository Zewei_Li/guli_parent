package com.example.edu.controller;

import com.example.utils.CommonResult;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

/**
 * @author 李泽伟
 */
@RestController
//@CrossOrigin
@RequestMapping("/eduservice/user")
@Api(description = "登录相关接口")
public class EduLoginController {

    @PostMapping(value = "login")
    public CommonResult login(){
        return CommonResult.ok().data("token","admin");
    }

    @GetMapping(value = "info")
    public CommonResult info(){
        return CommonResult.ok().data("roles","[admin]").data("name","admin").data("avatar","https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fi-1.lanrentuku.com%2F2020%2F11%2F5%2Fdef6ed04-6d34-402e-99c8-366266f627dd.png%3FimageView2%2F2%2Fw%2F500&refer=http%3A%2F%2Fi-1.lanrentuku.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1656167832&t=4bef4b0f5b26862727a4b19aa45e57ef");
    }
}
