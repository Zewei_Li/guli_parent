package com.example.edu.client;

import org.springframework.stereotype.Component;

@Component
public class OrderFileDegradeFeignClient implements OrderClient{

    @Override
    public boolean isBuyCourse(String courseId, String memberId) {
        System.out.println("调用订单服务失败");
        return false;
    }
}
