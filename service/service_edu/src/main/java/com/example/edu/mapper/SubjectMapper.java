package com.example.edu.mapper;

import com.example.edu.entity.Subject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程科目 Mapper 接口
 * </p>
 *
 * @author 李泽伟
 * @since 2022-05-27
 */
public interface SubjectMapper extends BaseMapper<Subject> {

}
