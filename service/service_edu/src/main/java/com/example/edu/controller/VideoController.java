package com.example.edu.controller;


import com.example.base.handler.Exception.GuliException;
import com.example.edu.client.VodClient;
import com.example.edu.entity.Video;
import com.example.edu.service.VideoService;
import com.example.utils.CommonResult;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 课程视频 前端控制器
 * </p>
 *
 * @author 李泽伟
 * @since 2022-05-28
 */
@RestController
@RequestMapping("/eduservice/video")
//@CrossOrigin
@Api(description = "小节相关接口")
public class VideoController {

    @Resource
    private VideoService videoService;

    @Resource
    private VodClient vodClient;

    //添加小节
    @PostMapping("addVideo")
    public CommonResult addVideo(@RequestBody Video video) {
        videoService.save(video);
        return CommonResult.ok();
    }

    // 删除小节，同时把里面视频删除
    @DeleteMapping("{id}")
    public CommonResult deleteVideo(@PathVariable String id) {
        //1.通过id获得视频id
        Video video=videoService.getById(id);
        String vid=video.getVideoSourceId();
        //2.先注入Feign上的vod服务
        //3.调用vod的服务删除视频
        CommonResult commonResult=vodClient.removeAlyVideo(vid);
        if (commonResult.getCode()==20001){
            throw new GuliException(20001,"Hystri，删除视频失败！");
        }
        //4.再删除小节
        videoService.removeById(id);
        return CommonResult.ok();
    }

}

