package com.example.edu.controller;


import com.example.edu.entity.Chapter;
import com.example.edu.entity.chapter.ChapterVo;
import com.example.edu.service.ChapterService;
import com.example.utils.CommonResult;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author 李泽伟
 * @since 2022-05-28
 */
@RestController
@RequestMapping("/eduservice/chapter")
//@CrossOrigin
@Api(description = "章节相关接口")
public class ChapterController {

    @Resource
    ChapterService chapterService;

    /**
     * 查询课程的所有章节和小节
     * @param courseId
     * @return
     */
    @GetMapping("getChapterVideo/{courseId}")
    public CommonResult getChapterVideo(@PathVariable String courseId){
        List<ChapterVo> list=chapterService.getChapterVideoByCourseId(courseId);
        return CommonResult.ok().data("allChapterVideo",list);
    }

    //添加章节
    @PostMapping("addChapter")
    public CommonResult addChapter(@RequestBody Chapter chapter) {
        chapterService.save(chapter);
        return CommonResult.ok();
    }

    //根据章节id查询
    @GetMapping("getChapterInfo/{chapterId}")
    public CommonResult getChapterInfo(@PathVariable String chapterId) {
        Chapter chapter = chapterService.getById(chapterId);
        return CommonResult.ok().data("chapter",chapter);
    }

    //修改章节
    @PostMapping("updateChapter")
    public CommonResult updateChapter(@RequestBody Chapter chapter) {
        chapterService.updateById(chapter);
        return CommonResult.ok();
    }

    //删除的方法
    @DeleteMapping("{chapterId}")
    public CommonResult deleteChapter(@PathVariable String chapterId) {
        boolean flag = chapterService.deleteChapter(chapterId);
        if(flag) {
            return CommonResult.ok();
        } else {
            return CommonResult.error();
        }

    }

}

