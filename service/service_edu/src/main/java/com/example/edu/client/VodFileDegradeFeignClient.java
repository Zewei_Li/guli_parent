package com.example.edu.client;

import com.example.utils.CommonResult;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class VodFileDegradeFeignClient implements VodClient{
    @Override
    public CommonResult removeAlyVideo(String id) {
        return CommonResult.error().message("Hystrix，删除视频失败");
    }

    @Override
    public CommonResult deleteBatch(List<String> ids) {
        return CommonResult.error().message("Hystrix，删除课程视频失败");
    }
}
