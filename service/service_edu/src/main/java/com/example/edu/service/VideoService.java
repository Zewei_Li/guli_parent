package com.example.edu.service;

import com.example.edu.entity.Video;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程视频 服务类
 * </p>
 *
 * @author 李泽伟
 * @since 2022-05-28
 */
public interface VideoService extends IService<Video> {

    void removeVideoByCourseId(String courseId);
}
