package com.example.edu.controller;


import com.example.edu.entity.subject.OneSubject;
import com.example.edu.service.SubjectService;
import com.example.utils.CommonResult;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 课程科目 前端控制器
 * </p>
 *
 * @author 李泽伟
 * @since 2022-05-27
 */
@RestController
@RequestMapping("/eduservice/subject")
//@CrossOrigin
@Api(description = "课程分类相关接口")
public class SubjectController {
    @Resource
    SubjectService subjectService;

    @PostMapping("addSubject")
    public CommonResult addSubject(MultipartFile file){
        subjectService.importSubjectData(file,subjectService);
        return CommonResult.ok();
    }

    @GetMapping("getAllSubject")
    public CommonResult getAllSubject(){
        List<OneSubject> list=subjectService.getAllOneTwoSubject();
        return CommonResult.ok().data("list",list);
    }

}

