package com.example.edu.mapper;

import com.example.edu.entity.Chapter;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author 李泽伟
 * @since 2022-05-28
 */
public interface ChapterMapper extends BaseMapper<Chapter> {

}
