package com.example.edu.controller.front;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.edu.client.OrderClient;
import com.example.edu.entity.Course;
import com.example.edu.entity.chapter.ChapterVo;
import com.example.edu.entity.frontvo.CourseFrontVo;
import com.example.edu.entity.frontvo.CourseWebVo;
import com.example.edu.service.ChapterService;
import com.example.edu.service.CourseService;
import com.example.utils.CommonResult;
import com.example.utils.JwtUtils;
import com.example.utils.ordervo.CourseWebVoOrder;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/eduservice/coursefront")
//@CrossOrigin
public class CourseFrontController {
    @Resource
    CourseService courseService;
    @Resource
    ChapterService chapterService;
    @Resource
    OrderClient orderClient;
    //条件查询带分页
    @PostMapping(value = "getFrontCourseList/{page}/{limit}")
    public CommonResult getFrontCourseList(@PathVariable Long page,
                                           @PathVariable Long limit,
                                           @RequestBody CourseFrontVo courseFrontVo){
        //1.封装分页条件
        Page<Course> pageCourse=new Page<>(page,limit);
        //2.分页条件+查询条件
        Map<String,Object> map=courseService.getCourseFrontList(pageCourse,courseFrontVo);
        return CommonResult.ok().data(map);
    }

    //课程详情页查询
    @GetMapping(value = "getFrontCourseInfo/{courseId}")
    public CommonResult getFrontCourseInfo(@PathVariable String courseId, HttpServletRequest httpServletRequest){
        //1.根据课程id查询：课程基本信息，课程描述，讲师信息，课程分类信息
        CourseWebVo courseWebVo=courseService.getBaseCourseInfo(courseId);
        //2.根据课程id查询：章节，小节信息
        List<ChapterVo> chapterVideoList = chapterService.getChapterVideoByCourseId(courseId);
        //3.根据课程id，用户id调用order-service，查询订单状态
        boolean buyCourse = orderClient.isBuyCourse(courseId, JwtUtils.getMemberIdByJwtToken(httpServletRequest));
        System.out.println("1++++++++++++++++++++++++++++++++++++++++++++++++++++++++"+buyCourse);
        return CommonResult.ok().data("courseWebVo",courseWebVo).data("chapterVideoList",chapterVideoList).data("isBuy",buyCourse);
    }

    //根据课程id查询课程信息
    @GetMapping(value = "getCourseInfoOrder/{id}")
    public CourseWebVoOrder getCourseInfoOrder(@PathVariable String id){
        CourseWebVo baseCourseInfo = courseService.getBaseCourseInfo(id);
        CourseWebVoOrder courseWebVoOrder=new CourseWebVoOrder();
        BeanUtils.copyProperties(baseCourseInfo,courseWebVoOrder);
        return  courseWebVoOrder;
    }
}
