package com.example.edu.mapper;

import com.example.edu.entity.Course;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.edu.entity.frontvo.CourseWebVo;
import com.example.edu.entity.vo.CoursePublishVo;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author 李泽伟
 * @since 2022-05-28
 */
public interface CourseMapper extends BaseMapper<Course> {
    public CoursePublishVo getPublishCourseInfo(String courseId);

    CourseWebVo getBaseCourseInfo();

    CourseWebVo getBaseCourseInfo(String courseId);
}
