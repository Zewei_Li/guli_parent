package com.example.edu.service;

import com.example.edu.entity.Chapter;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.edu.entity.chapter.ChapterVo;

import java.util.List;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author 李泽伟
 * @since 2022-05-28
 */
public interface ChapterService extends IService<Chapter> {

    //查询课程的所有章节和小节
    List<ChapterVo> getChapterVideoByCourseId(String courseId);

    //删除的方法
    boolean deleteChapter(String chapterId);

    void removeChapterByCourseId(String courseId);
}
