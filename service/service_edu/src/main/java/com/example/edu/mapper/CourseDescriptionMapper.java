package com.example.edu.mapper;

import com.example.edu.entity.CourseDescription;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程简介 Mapper 接口
 * </p>
 *
 * @author 李泽伟
 * @since 2022-05-28
 */
public interface CourseDescriptionMapper extends BaseMapper<CourseDescription> {

}
