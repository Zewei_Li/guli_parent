package com.example.edu.service;

import com.example.edu.entity.CourseDescription;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程简介 服务类
 * </p>
 *
 * @author 李泽伟
 * @since 2022-05-28
 */
public interface CourseDescriptionService extends IService<CourseDescription> {

}
