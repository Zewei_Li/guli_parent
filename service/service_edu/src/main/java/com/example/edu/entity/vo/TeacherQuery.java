package com.example.edu.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 李泽伟
 */
@Data
public class TeacherQuery {
    @ApiModelProperty(value = "教师名词，模糊查询")
    private String name;
    @ApiModelProperty(value = "头衔：1高级讲师 2首席讲师")
    private Integer level;
    /**
     * 使用String类型，前端传过来的数据无需进行类型转换
     */
    private String begin;
    private String end;
}
