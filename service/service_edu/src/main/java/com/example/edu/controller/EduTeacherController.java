package com.example.edu.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.edu.entity.Teacher;
import com.example.edu.entity.vo.TeacherQuery;
import com.example.edu.service.TeacherService;
import com.example.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author 李泽伟
 * @since 2022-05-25
 */
@Api(description = "讲师相关接口")
@RestController
//@CrossOrigin
@RequestMapping("/eduservice/teacher")
public class EduTeacherController {

    @Resource
    private TeacherService teacherService;

    /**
     * 查询所有讲师方法
     * @return
     */
    @ApiOperation(value = "查询所有讲师信息")
    @GetMapping(value = "/findAll")
    public CommonResult findAllTeacher(){
        return  CommonResult.ok().data("items",teacherService.list(null));
    }

    /**
     * 逻辑删除讲师方法
     * @param id
     * @return
     */
    @ApiOperation(value = "根据id删除讲师信息")
    @DeleteMapping(value = "{id}")
    public CommonResult removeById(@PathVariable String id){
        System.out.println("来了---根据id删除讲师信息");
        boolean flag=teacherService.removeById(id);
        if(flag){
            return CommonResult.ok().message("删除成功");
        }else{
            return CommonResult.error().message("删除失败");
        }
    }

    /**
     * 根据当前页号和页大小进行分页查询
     * @param current
     * @param limit
     * @return
     */
    @ApiOperation(value = "根据当前页号和页大小进行分页查询")
    @GetMapping(value = "/limitPage/{current}/{limit}")
    public CommonResult limitPage(@PathVariable Long current,
                                  @PathVariable Long limit){
        Page<Teacher> limitPage=new Page<>(current,limit);
        teacherService.page(limitPage,null);

        return CommonResult.ok().data("total",limitPage.getTotal()).data("rows",limitPage.getRecords());
    }

    /**
     * 多条件分页查询
     * @param current
     * @param limit
     * @param teacherQuery
     * @return
     */
    @ApiOperation(value = "多条件分页查询")
    @PostMapping(value = "/pageTeacherCondition/{current}/{limit}")
    public CommonResult pageTeacherCondition(@PathVariable Long current,
                                    @PathVariable Long limit,
                                    @RequestBody TeacherQuery teacherQuery){
        Page<Teacher> limitPage=new Page<>(current,limit);
        QueryWrapper queryWrapper=new QueryWrapper();
        String begin = teacherQuery.getBegin();
        String end = teacherQuery.getEnd();
        Integer level = teacherQuery.getLevel();
        String name = teacherQuery.getName();
        if(!StringUtils.isEmpty(begin)) {
            queryWrapper.ge("gmt_create",begin);
        }
        if(!StringUtils.isEmpty(end)) {
            queryWrapper.le("gmt_modified",end);
        }
        if(!StringUtils.isEmpty(level)) {
            queryWrapper.eq("level",level);
        }
        if(!StringUtils.isEmpty(name)) {
            queryWrapper.like("name",name);
        }
        queryWrapper.orderByDesc("gmt_create");
        teacherService.page(limitPage,queryWrapper);
        System.out.println(limitPage.getRecords());

        return CommonResult.ok().data("total",limitPage.getTotal()).data("rows",limitPage.getRecords());
    }

    @ApiOperation(value = "通过id查询")
    @GetMapping(value = "/getTeacher/{id}")
    public CommonResult getTeacher(@PathVariable String id){
        System.out.println("来了---通过id查询");
        Teacher teacher = teacherService.getById(id);
        return CommonResult.ok().data("teacher",teacher);
    }

    @ApiOperation(value = "新增讲师")
    @PostMapping(value = "/addTeacher")
    public CommonResult addTeacher(@RequestBody Teacher teacher){
        boolean save = teacherService.save(teacher);
        if(save){
            return CommonResult.ok().message("新增成功");
        }else{
            return CommonResult.error().message("新增失败");
        }
    }

    @ApiOperation(value = "修改讲师")
    @PostMapping(value = "/updateTeacher")
    public CommonResult updateTeacher(@RequestBody Teacher teacher){
        /*try{
            int a = 1 / 0;
        }catch (Exception e){
            throw new GuliException(20001,"自定义异常");
        }*/
        boolean update = teacherService.updateById(teacher);
        if(update){
            return CommonResult.ok().message("修改成功");
        }else{
            return CommonResult.error().message("修改失败");
        }
    }

}