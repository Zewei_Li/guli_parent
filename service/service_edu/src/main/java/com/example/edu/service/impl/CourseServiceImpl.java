package com.example.edu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.base.handler.Exception.GuliException;
import com.example.edu.entity.Course;
import com.example.edu.entity.CourseDescription;
import com.example.edu.entity.frontvo.CourseFrontVo;
import com.example.edu.entity.frontvo.CourseWebVo;
import com.example.edu.entity.vo.CourseInfoForm;
import com.example.edu.entity.vo.CoursePublishVo;
import com.example.edu.mapper.CourseMapper;
import com.example.edu.service.ChapterService;
import com.example.edu.service.CourseDescriptionService;
import com.example.edu.service.CourseService;
import com.example.edu.service.VideoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author 李泽伟
 * @since 2022-05-28
 */
@Service
public class CourseServiceImpl extends ServiceImpl<CourseMapper, Course> implements CourseService {
    @Autowired
    CourseDescriptionService courseDescriptionService;
    @Autowired
    ChapterService chapterService;
    @Autowired
    VideoService videoService;

    //添加课程信息
    @Override
    public String saveCourseInfo(CourseInfoForm courseInfoForm) {
        //1.将课程信息添加到课程表中
        Course course=new Course();
        BeanUtils.copyProperties(courseInfoForm,course);
        int insert = baseMapper.insert(course);
        String cid = course.getId();
        if (insert == 0) {
            throw new GuliException(20001,"课程信息插入失败");
        }
        //2.将课程简介添加到课程简介表中
        CourseDescription courseDescription=new CourseDescription();
        courseDescription.setDescription(courseInfoForm.getDescription());
        courseDescription.setId(cid);
        courseDescriptionService.save(courseDescription);
        return cid;
    }

    //通过id查询课程信息
    @Override
    public CourseInfoForm getCourseInfo(String courseId) {
        CourseInfoForm courseInfoForm=new CourseInfoForm();
        //1.封装基本信息
        Course course = baseMapper.selectById(courseId);
        BeanUtils.copyProperties(course,courseInfoForm);
        //2.封装简介信息
        CourseDescription courseDescription = courseDescriptionService.getById(courseId);
        courseInfoForm.setDescription(courseDescription.getDescription());
        return courseInfoForm;
    }

    //修改课程信息
    @Override
    public void updateCourseInfo(CourseInfoForm courseInfoForm) {
        //1.封装基本信息
        Course course=new Course();
        BeanUtils.copyProperties(courseInfoForm,course);
        //2.封装描述信息
        CourseDescription courseDescription=new CourseDescription();
        courseDescription.setDescription(courseInfoForm.getDescription());
        courseDescription.setId(courseInfoForm.getId());
        //3.进行数据库修改
        int i = baseMapper.updateById(course);
        if (i == 0) {
            throw new GuliException(20001,"课程修改失败");
        }
        courseDescriptionService.updateById(courseDescription);
    }

    @Override
    public CoursePublishVo publishCourseInfo(String id) {
        CoursePublishVo coursePublishVo=baseMapper.getPublishCourseInfo(id);
        return coursePublishVo;
    }

    @Override
    public void removeCourse(String courseId) {
        //1 根据课程id删除小节
        videoService.removeVideoByCourseId(courseId);

        //2 根据课程id删除章节
        chapterService.removeChapterByCourseId(courseId);

        //3 根据课程id删除描述
        courseDescriptionService.removeById(courseId);

        //4 根据课程id删除课程本身
        int result = baseMapper.deleteById(courseId);
        if(result == 0) { //失败返回
            throw new GuliException(20001,"删除失败");
        }
    }

    @Override
    public Map<String, Object> getCourseFrontList(Page<Course> pageCourse, CourseFrontVo courseFrontVo) {
        //1.封装条件
        QueryWrapper queryWrapper=new QueryWrapper();
        //1.1判断一级分类是否为空
        if(!StringUtils.isEmpty(courseFrontVo.getSubjectParentId())){
            queryWrapper.eq("subject_parent_id",courseFrontVo.getSubjectParentId());
        }
        //1.2判断二级分类是否为空
        if(!StringUtils.isEmpty(courseFrontVo.getSubjectId())){
            queryWrapper.eq("subject_id",courseFrontVo.getSubjectId());
        }
        //1.3判断关注度排序是否为空
        if(!StringUtils.isEmpty(courseFrontVo.getBuyCountSort())){
            queryWrapper.orderByDesc("buy_count",courseFrontVo.getSubjectParentId());
        }
        //1.4判断最新排序是否为空
        if(!StringUtils.isEmpty(courseFrontVo.getGmtCreateSort())){
            queryWrapper.orderByDesc("gmt_create",courseFrontVo.getSubjectParentId());
        }
        //1.5判断价格排序是否为空
        if(!StringUtils.isEmpty(courseFrontVo.getPriceSort())){
            queryWrapper.orderByDesc("price",courseFrontVo.getSubjectParentId());
        }

        //2.查询出分页数据
        baseMapper.selectPage(pageCourse, queryWrapper);

        //3.封装分页数据
        List<Course> records = pageCourse.getRecords();
        long current = pageCourse.getCurrent();
        long pages = pageCourse.getPages();
        long size = pageCourse.getSize();
        long total = pageCourse.getTotal();
        boolean hasNext = pageCourse.hasNext();//下一页
        boolean hasPrevious = pageCourse.hasPrevious();//上一页

        //4.返回map
        Map<String, Object> map = new HashMap<>();
        map.put("items", records);
        map.put("current", current);
        map.put("pages", pages);
        map.put("size", size);
        map.put("total", total);
        map.put("hasNext", hasNext);
        map.put("hasPrevious", hasPrevious);
        return map;
    }

    @Override
    public CourseWebVo getBaseCourseInfo(String courseId) {
        CourseWebVo courseWebVo=baseMapper.getBaseCourseInfo(courseId);
        return courseWebVo;
    }
}
