package com.example.edu.client;

import com.example.utils.CommonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name="service-vod",fallback = VodFileDegradeFeignClient.class)
@Component
public interface VodClient {

    //删除阿里云上的视频
    @DeleteMapping(value = "/eduvod/video/removeAlyVideo/{id}")
    public CommonResult removeAlyVideo(@PathVariable("id") String id);

    //删除课程-删除课程中所有视频
    @DeleteMapping(value = "/eduvod/video/delete-batch")
    public CommonResult deleteBatch(@RequestParam("ids") List<String> ids);
}
