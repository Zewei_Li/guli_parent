package com.example.edu.service.impl;

import com.example.edu.entity.CourseDescription;
import com.example.edu.mapper.CourseDescriptionMapper;
import com.example.edu.service.CourseDescriptionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程简介 服务实现类
 * </p>
 *
 * @author 李泽伟
 * @since 2022-05-28
 */
@Service
public class CourseDescriptionServiceImpl extends ServiceImpl<CourseDescriptionMapper, CourseDescription> implements CourseDescriptionService {

}
