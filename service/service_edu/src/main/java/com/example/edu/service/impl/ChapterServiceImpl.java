package com.example.edu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.base.handler.Exception.GuliException;
import com.example.edu.entity.Chapter;
import com.example.edu.entity.Video;
import com.example.edu.entity.chapter.ChapterVo;
import com.example.edu.entity.chapter.VideoVo;
import com.example.edu.mapper.ChapterMapper;
import com.example.edu.service.ChapterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.edu.service.VideoService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author 李泽伟
 * @since 2022-05-28
 */
@Service
public class ChapterServiceImpl extends ServiceImpl<ChapterMapper, Chapter> implements ChapterService {
    @Resource
    VideoService videoService;

    /**
     * 查询课程的所有章节和小节
     * @param courseId
     * @return
     */
    @Override
    public List<ChapterVo> getChapterVideoByCourseId(String courseId) {
        //1.查询该课程所有章节
        QueryWrapper chapterWrapper=new QueryWrapper();
        chapterWrapper.eq("course_id",courseId);
        List<Chapter> chapters = baseMapper.selectList(chapterWrapper);
        //2.查询该课程所有小结
        QueryWrapper VideoWrapper=new QueryWrapper();
        VideoWrapper.eq("course_id",courseId);
        List<Video> Videos=videoService.list(VideoWrapper);

        //3.将所有章节进行封装
        List<ChapterVo> finalChapters=new ArrayList<>();
        for (Chapter chapter : chapters) {
            ChapterVo chapterVo=new ChapterVo();
            BeanUtils.copyProperties(chapter,chapterVo);

            //4.循环所有小结，将小结封装进所属章节
            List<VideoVo> finalVideos=new ArrayList<>();
            for (Video video : Videos) {
                if(video.getChapterId().equals(chapter.getId())){
                    VideoVo videoVo=new VideoVo();
                    BeanUtils.copyProperties(video,videoVo);
                    finalVideos.add(videoVo);
                }
            }
            chapterVo.setChildren(finalVideos);
            finalChapters.add(chapterVo);
        }

        return finalChapters;
    }

    /**
     * 根据id删除章节
     * @param chapterId
     * @return
     */
    @Override
    public boolean deleteChapter(String chapterId) {
        //根据chapterid章节id 查询小节表，如果查询数据，不进行删除
        QueryWrapper<Video> wrapper = new QueryWrapper<>();
        wrapper.eq("chapter_id",chapterId);
        int count = videoService.count(wrapper);
        //判断
        if(count >0) {//查询出小节，不进行删除
            throw new GuliException(20001,"不能删除");
        } else { //不能查询数据，进行删除
            //删除章节
            int result = baseMapper.deleteById(chapterId);
            //成功  1>0   0>0
            return result>0;
        }
    }

    //2 根据课程id删除章节
    @Override
    public void removeChapterByCourseId(String courseId) {
        QueryWrapper<Chapter> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id",courseId);
        baseMapper.delete(wrapper);
    }
}
