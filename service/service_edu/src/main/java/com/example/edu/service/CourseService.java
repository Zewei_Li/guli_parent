package com.example.edu.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.edu.entity.Course;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.edu.entity.frontvo.CourseFrontVo;
import com.example.edu.entity.frontvo.CourseWebVo;
import com.example.edu.entity.vo.CourseInfoForm;
import com.example.edu.entity.vo.CoursePublishVo;

import java.util.Map;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author 李泽伟
 * @since 2022-05-28
 */
public interface CourseService extends IService<Course> {

    //添加课程信息
    String saveCourseInfo(CourseInfoForm courseInfoForm);

    //通过id查询课程信息
    CourseInfoForm getCourseInfo(String courseId);

    //修改课程信息
    void updateCourseInfo(CourseInfoForm courseInfoForm);

    CoursePublishVo publishCourseInfo(String id);

    void removeCourse(String courseId);

    Map<String, Object> getCourseFrontList(Page<Course> pageCourse, CourseFrontVo courseFrontVo);

    CourseWebVo getBaseCourseInfo(String courseId);
}
