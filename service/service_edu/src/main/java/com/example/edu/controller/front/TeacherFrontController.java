package com.example.edu.controller.front;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.edu.entity.Course;
import com.example.edu.entity.Teacher;
import com.example.edu.service.CourseService;
import com.example.edu.service.TeacherService;
import com.example.utils.CommonResult;
import javafx.beans.binding.LongExpression;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("eduservice/teacherfront")
//@CrossOrigin
public class TeacherFrontController {
    @Resource
    TeacherService teacherService;
    @Resource
    CourseService courseService;

    //讲师列表分页
    @PostMapping("getTeacherFrontList/{page}/{limit}")
    public CommonResult getTeacherFrontList(@PathVariable Long page, @PathVariable Long limit){
        Page<Teacher> pageTeacher=new Page(page,limit);
        Map<String,Object> map=teacherService.getTeacherFrontList(pageTeacher);
        return CommonResult.ok().data(map);
    }

    //讲师详情信息
    @GetMapping("getTeacherFrontInfo/{id}")
    public CommonResult getTeacherFrontInfo(@PathVariable String id){
        //1.查询讲师信息
        Teacher teacher = teacherService.getById(id);
        //2.查询所讲课程信息
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("teacher_id",id);
        List<Course> list = courseService.list(queryWrapper);
        return CommonResult.ok().data("teacher",teacher).data("courseList",list);
    }
}
