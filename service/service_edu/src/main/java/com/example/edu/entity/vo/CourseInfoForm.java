package com.example.edu.entity.vo;

import com.example.edu.entity.Course;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CourseInfoForm extends Course {
    @ApiModelProperty(value = "课程简介")
    private String description;
}
