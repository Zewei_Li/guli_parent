package com.example.edu.controller;


import com.example.edu.entity.Course;
import com.example.edu.entity.vo.CourseInfoForm;
import com.example.edu.entity.vo.CoursePublishVo;
import com.example.edu.service.CourseService;
import com.example.utils.CommonResult;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author 李泽伟
 * @since 2022-05-28
 */
@RestController
@RequestMapping("/eduservice/course")
//@CrossOrigin
@Api(description = "课程相关接口")
public class CourseController {

    @Resource
    CourseService courseService;

    //课程列表 基本实现
    //TODO  完善条件查询带分页

    /**
     * 查询所有课程信息
     * @return
     */
    @GetMapping
    public CommonResult getCourseList() {
        List<Course> list = courseService.list(null);
        return CommonResult.ok().data("list",list);
    }

    /**
     * 添加课程信息
     * @param courseInfoForm
     * @return
     */
    @PostMapping(value = "addCourseInfo")
    public CommonResult saveCourseInfo(@RequestBody CourseInfoForm courseInfoForm){
        String id=courseService.saveCourseInfo(courseInfoForm);
        return CommonResult.ok().data("courseId",id);
    }

    /**
     * 通过id查询课程信息
     * @param courseId
     * @return
     */
    @GetMapping(value = "getCourseInfo/{courseId}")
    public CommonResult getCourseInfo(@PathVariable String courseId){
        CourseInfoForm courseInfoForm= courseService.getCourseInfo(courseId);
        return CommonResult.ok().data("courseInfoVo",courseInfoForm);
    }

    /**
     * 修改课程信息
     * @param courseInfoForm
     * @return
     */
    @PostMapping(value = "updateCourseInfo")
    public CommonResult updateCourseInfo(@RequestBody CourseInfoForm courseInfoForm){
        courseService.updateCourseInfo(courseInfoForm);
        return CommonResult.ok();
    }

    /**
     * 获得课程最终发布信息
     * @param id
     * @return
     */
    @GetMapping("getPublishCourseInfo/{id}")
    public CommonResult getPublishCourseInfo(@PathVariable String id){
        CoursePublishVo coursePublishVo=courseService.publishCourseInfo(id);
        return CommonResult.ok().data("publishCourse",coursePublishVo);
    }

    /**
     * 提交课程发布，修改课程状态为“Normal”
     * @param id
     * @return
     */
    @PostMapping(value = "publishCourse/{id}")
    public CommonResult publishCourse(@PathVariable String id){
        Course course=new Course();
        course.setId(id);
        course.setStatus("Normal");
        courseService.updateById(course);
        return CommonResult.ok();
    }

    /**
     * 通过id删除课程
     * @param courseId
     * @return
     */
    @DeleteMapping("{courseId}")
    public CommonResult deleteCourse(@PathVariable String courseId) {
        courseService.removeCourse(courseId);
        return CommonResult.ok();
    }

}

