package com.example.edu.mapper;

import com.example.edu.entity.Teacher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 讲师 Mapper 接口
 * </p>
 *
 * @author 李泽伟
 * @since 2022-05-25
 */
public interface TeacherMapper extends BaseMapper<Teacher> {

}
