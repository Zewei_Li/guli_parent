package com.example.order.mapper;

import com.example.order.entity.PayLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 支付日志表 Mapper 接口
 * </p>
 *
 * @author 李泽伟
 * @since 2022-06-03
 */
public interface PayLogMapper extends BaseMapper<PayLog> {

}
