package com.example.order.client;

import com.example.utils.ordervo.UcenterMemberOrder;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Component
@FeignClient(name = "service-ucenter")
public interface UcenterClient {

    //根据用户id获得用户信息
    @PostMapping(value = "/api/ucenter/member/getUserInfoOrder/{id}")
    public abstract UcenterMemberOrder getUserInfoOrder(@PathVariable("id") String id);
}
