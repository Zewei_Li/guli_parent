package com.example.order.service;

import com.example.order.entity.PayLog;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 支付日志表 服务类
 * </p>
 *
 * @author 李泽伟
 * @since 2022-06-03
 */
public interface PayLogService extends IService<PayLog> {

    //生成微信支付二维码
    Map createNatvie(String orderNo);

    //查询订单状态
    Map<String, String> queryPayStatus(String orderNo);

    //支付成功，新增支付记录，更新订单信息
    void updateOrdersStatus(Map<String, String> map);
}
