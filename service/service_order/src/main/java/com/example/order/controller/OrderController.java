package com.example.order.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.order.entity.Order;
import com.example.order.service.OrderService;
import com.example.utils.CommonResult;
import com.example.utils.JwtUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 订单 前端控制器
 * </p>
 *
 * @author 李泽伟
 * @since 2022-06-03
 */
@RestController
@RequestMapping("/eduorder/order")
//@CrossOrigin
public class OrderController {
    @Resource
    OrderService orderService;
    //通过课程id和token生成订单
    @PostMapping(value = "createOrder/{courseId}")
    public CommonResult createOrder(@PathVariable String courseId, HttpServletRequest httpServletRequest){
        String orderNo=orderService.createOrder(courseId, JwtUtils.getMemberIdByJwtToken(httpServletRequest));
        return CommonResult.ok().data("orderId",orderNo);
    }

    //通过订单编号查询订单信息
    @GetMapping(value = "getOrderInfo/{id}")
    public CommonResult getOrderInfo(@PathVariable String id){
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("order_no",id);
        Order order = orderService.getOne(queryWrapper);
        return CommonResult.ok().data("item",order);
    }

    //查询订单状态
    @GetMapping(value = "isBuyCourse/{courseId}/{memberId}")
    public boolean isBuyCourse(@PathVariable String courseId,@PathVariable String memberId){
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("course_id",courseId);
        queryWrapper.eq("member_id",memberId);
        queryWrapper.eq("status",1);
        int count = orderService.count(queryWrapper);
        if (count>0){
            return true;
        }else {
            return false;
        }
    }

}

