package com.example.order.controller;


import com.example.order.service.OrderService;
import com.example.order.service.PayLogService;
import com.example.utils.CommonResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * <p>
 * 支付日志表 前端控制器
 * </p>
 *
 * @author 李泽伟
 * @since 2022-06-03
 */
@RestController
@RequestMapping("/eduorder/paylog")
//@CrossOrigin
public class PayLogController {
    @Resource
    PayLogService payLogService;

    //生成微信支付二维码
    @GetMapping(value = "createNative/{orderNo}")
    public CommonResult createNative(@PathVariable String orderNo){
        Map map=payLogService.createNatvie(orderNo);
        System.out.println("======================="+map);

        return CommonResult.ok().data(map);
    }

    //查询支付状态
    @GetMapping(value = "queryPayStatus/{orderNo}")
    public CommonResult queryPayStatus(@PathVariable String orderNo){
        //1 查询订单状态
        Map<String,String> map=payLogService.queryPayStatus(orderNo);
        System.out.println("======================="+map);

        //2 状态为空，返回错误信息
        if (map == null) {
            return CommonResult.error().message("支付出错");
        }

        //3 如果支付成功，新增支付记录，更新订单信息
        if(map.get("trade_state").equals("SUCCESS")){
            payLogService.updateOrdersStatus(map);
            return CommonResult.ok().message("支付成功");
        }

        //4 否则返回正在支付中
        return CommonResult.ok().code(25000).message("支付中");
    }


}

