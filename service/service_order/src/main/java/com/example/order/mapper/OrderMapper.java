package com.example.order.mapper;

import com.example.order.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单 Mapper 接口
 * </p>
 *
 * @author 李泽伟
 * @since 2022-06-03
 */
public interface OrderMapper extends BaseMapper<Order> {

}
