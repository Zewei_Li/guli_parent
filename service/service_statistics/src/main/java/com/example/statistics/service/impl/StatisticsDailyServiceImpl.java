package com.example.statistics.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.statistics.client.UcenterClient;
import com.example.statistics.entity.StatisticsDaily;
import com.example.statistics.mapper.StatisticsDailyMapper;
import com.example.statistics.service.StatisticsDailyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.utils.CommonResult;
import io.swagger.models.auth.In;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 网站统计日数据 服务实现类
 * </p>
 *
 * @author 李泽伟
 * @since 2022-06-03
 */
@Service
public class StatisticsDailyServiceImpl extends ServiceImpl<StatisticsDailyMapper, StatisticsDaily> implements StatisticsDailyService {

    @Resource
    private UcenterClient ucenterClient;

    //生成某天注册人数的记录
    @Override
    public void registerCount(String day) {
        //添加记录之前删除表相同日期的数据
        QueryWrapper<StatisticsDaily> wrapper = new QueryWrapper<>();
        wrapper.eq("date_calculated",day);
        baseMapper.delete(wrapper);

        //1 获得这一天注册用户的人数
        CommonResult commonResult = ucenterClient.countRegister(day);
        Integer count = (Integer) commonResult.getData().get("countRegister");

        //2 封装需要统计的数据
        StatisticsDaily statisticsDaily=new StatisticsDaily();
        statisticsDaily.setRegisterNum(count);
        statisticsDaily.setDateCalculated(day);
        statisticsDaily.setVideoViewNum(RandomUtils.nextInt(100,200));
        statisticsDaily.setLoginNum(RandomUtils.nextInt(100,200));
        statisticsDaily.setCourseNum(RandomUtils.nextInt(100,200));
        baseMapper.insert(statisticsDaily);
    }

    //图表显示，返回日期，数量两部分数据
    @Override
    public Map<String, Object> showData(String type, String begin, String end) {
        //1 通过类型，时间查询信息
        QueryWrapper<StatisticsDaily> queryWrapper=new QueryWrapper<>();
        queryWrapper.between("date_calculated",begin,end);
        queryWrapper.select("date_calculated",type);
        List<StatisticsDaily> stas = baseMapper.selectList(queryWrapper);

        //2 判断需要的数据类型，将数控封装到两个list集合中
        List<String> dateList=new ArrayList<>();
        List<Integer> numList=new ArrayList<>();
        for (StatisticsDaily sta : stas) {
            dateList.add(sta.getDateCalculated());
            switch (type){
                case "register_num":
                    numList.add(sta.getRegisterNum());
                    break;
                case "login_num":
                    numList.add(sta.getLoginNum());
                    break;
                case "video_view_num":
                    numList.add(sta.getVideoViewNum());
                    break;
                case "course_num":
                    numList.add(sta.getCourseNum());
                    break;
                default:
                    break;
            }
        }

        //3 将两个list集合封装到map中进行返回
        Map<String,Object> map=new HashMap<>();
        map.put("date_calculatedList",dateList);
        map.put("numDataList",numList);
        return map;
    }
}
