package com.example.statistics.client;

import com.example.utils.CommonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("service-ucenter")
@Component
public interface UcenterClient {
    //查询某一天用户的注册数量
    @GetMapping(value = "/api/ucenter/member/countRegister/{day}")
    public CommonResult countRegister(@PathVariable String day);
}
