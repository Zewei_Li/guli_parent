package com.example.statistics.controller;


import com.example.statistics.client.UcenterClient;
import com.example.statistics.service.StatisticsDailyService;
import com.example.utils.CommonResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * <p>
 * 网站统计日数据 前端控制器
 * </p>
 *
 * @author 李泽伟
 * @since 2022-06-03
 */
@RestController
@RequestMapping("/staservice/sta")
//@CrossOrigin
public class StatisticsDailyController {
    @Resource
    StatisticsDailyService statisticsDailyService;

    //生成某天注册人数的记录
    @PostMapping(value = "registerCount/{day}")
    public CommonResult registerCount(@PathVariable String day){
        statisticsDailyService.registerCount(day);
        return CommonResult.ok();
    }

    //图表显示，返回日期，数量两部分数据
    @GetMapping(value = "showData/{type}/{begin}/{end}")
    public CommonResult showData(@PathVariable String type,@PathVariable String begin,@PathVariable String end){
        Map<String,Object> map=statisticsDailyService.showData(type,begin,end);
        return CommonResult.ok().data(map);
    }
}

