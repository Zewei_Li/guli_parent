package com.example.statistics.mapper;

import com.example.statistics.entity.StatisticsDaily;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 网站统计日数据 Mapper 接口
 * </p>
 *
 * @author 李泽伟
 * @since 2022-06-03
 */
public interface StatisticsDailyMapper extends BaseMapper<StatisticsDaily> {

}
