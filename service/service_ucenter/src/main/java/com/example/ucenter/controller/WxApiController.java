package com.example.ucenter.controller;

import com.example.base.handler.Exception.GuliException;
import com.example.ucenter.entity.Member;
import com.example.ucenter.service.MemberService;
import com.example.ucenter.utils.ConstantWxUtils;
import com.example.ucenter.utils.HttpClientUtils;
import com.example.utils.JwtUtils;
import com.google.gson.Gson;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.net.URLEncoder;
import java.util.HashMap;

@Controller
//@CrossOrigin
@RequestMapping("/api/ucenter/wx")
public class WxApiController {
    @Resource
    MemberService memberService;

    /**
     * 微信扫码登录入口
     * @return
     */
    @GetMapping(value = "login")
    public String getWxCode(){
        String baseUrl = "https://open.weixin.qq.com/connect/qrconnect" +
                "?appid=%s" +
                "&redirect_uri=%s" +
                "&response_type=code" +
                "&scope=snsapi_login" +
                "&state=%s" +
                "#wechat_redirect";

        //对redirect_url进行URLEncoder编码
        String redirectUrl = ConstantWxUtils.WX_OPEN_REDIRECT_URL;
        try {
            redirectUrl = URLEncoder.encode(redirectUrl, "utf-8");
        }catch(Exception e) {
        }

        //设置%s里面值
        String url = String.format(
                baseUrl,
                ConstantWxUtils.WX_OPEN_APP_ID,
                redirectUrl,
                "atguigu"
        );

        //重定向到请求微信地址里面
        return "redirect:"+url;
    }

    /**
     * 扫码登录出口
     */
    @GetMapping(value = "callback")
    public String callback(String code,String state){
        try{
            //1.通过临时凭证code向腾讯发送请求，返回accsess_token 和 openid
            String baseAccessTokenUrl = "https://api.weixin.qq.com/sns/oauth2/access_token" +
                    "?appid=%s" +
                    "&secret=%s" +
                    "&code=%s" +
                    "&grant_type=authorization_code";
            //拼接三个参数 ：id  秘钥 和 code值
            String accessTokenUrl = String.format(
                    baseAccessTokenUrl,
                    ConstantWxUtils.WX_OPEN_APP_ID,
                    ConstantWxUtils.WX_OPEN_APP_SECRET,
                    code
            );
            String accessTokenInfo = HttpClientUtils.get(accessTokenUrl);

            //2.利用返回的accessToken再次向腾讯发送请求，获得用户信息
            //2.1 解析返回的数据获得access_token,openid
            Gson gson=new Gson();
            HashMap hashMap = gson.fromJson(accessTokenInfo, HashMap.class);
            String access_token= (String) hashMap.get("access_token");
            String openid= (String) hashMap.get("openid");

            //3.根据用户openid查询数据库，判断该用户是否存在
            Member member=memberService.getOpenIdMember(openid);
            if(member == null){
                //4.不存在利用access_token,openid再次向腾讯发送请求，获得用户信息并将用户信息添加进数据库
                String baseUserInfoUrl = "https://api.weixin.qq.com/sns/userinfo" +
                        "?access_token=%s" +
                        "&openid=%s";
                //拼接两个参数
                String userInfoUrl = String.format(
                        baseUserInfoUrl,
                        access_token,
                        openid
                );
                String userInfo = HttpClientUtils.get(userInfoUrl);
                HashMap userInfoMap = gson.fromJson(userInfo, HashMap.class);
                String nickname = (String)userInfoMap.get("nickname");//昵称
                String headimgurl = (String)userInfoMap.get("headimgurl");//头像
                member = new Member();
                member.setOpenid(openid);
                member.setNickname(nickname);
                member.setAvatar(headimgurl);
                memberService.save(member);
            }

            //5.跳转到C端
            String jwtToken = JwtUtils.getJwtToken(member.getId(), member.getNickname());
            //最后：返回首页面，通过路径传递token字符串
            return "redirect:http://192.168.10.31:3000?token="+jwtToken;
        }catch (Exception e){
            throw new GuliException(20001,"微信登录失败");
        }
    }
}
