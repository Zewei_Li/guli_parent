package com.example.ucenter.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.base.handler.Exception.GuliException;
import com.example.ucenter.entity.Member;
import com.example.ucenter.entity.vo.RegisterVo;
import com.example.ucenter.mapper.MemberMapper;
import com.example.ucenter.service.MemberService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.utils.JwtUtils;
import com.example.utils.MD5;
import io.swagger.models.auth.In;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author 李泽伟
 * @since 2022-05-31
 */
@Service
public class MemberServiceImpl extends ServiceImpl<MemberMapper, Member> implements MemberService {

    @Resource
    RedisTemplate<String,String> redisTemplate;

    /**
     * 用户登录，生成token
     * @param member
     * @return
     */
    @Override
    public String login(Member member) {
        //1.判断手机号或密码是否为空
        String mobile=member.getMobile();
        String password=member.getPassword();
        if(StringUtils.isEmpty(mobile) || StringUtils.isEmpty(password)){
            throw new GuliException(20001,"用户名或密码为空");
        }
        //2.通过手机号查询数据库
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("mobile",mobile);
        Member member1 = baseMapper.selectOne(queryWrapper);
        //3.判断用户是否存在
        if(member1 == null){
            throw new GuliException(20001,"用户不存在");
        }
        //4.判断加密后的密码是否正确
        if(!MD5.encrypt(password).equals(member1.getPassword())){
            throw new GuliException(20001,"密码错误");
        }
        //5.判断用户是否禁用
        if(member1.getIsDisabled()){
            throw new GuliException(20001,"用户被禁用");
        }
        //6.通过jwt生成token
        String jwtToken = JwtUtils.getJwtToken(member1.getId(), member1.getNickname());

        return jwtToken;
    }

    //用户注册
    @Override
    public void register(RegisterVo registerVo) {
        //1.判断用户名，手机，密码，验证码是否为空
        String nickName=registerVo.getNickname();
        String mobile=registerVo.getMobile();
        String password = registerVo.getPassword();
        String code = registerVo.getCode();
        if(StringUtils.isEmpty(nickName) || StringUtils.isEmpty(mobile) || StringUtils.isEmpty(password) || StringUtils.isEmpty(code)){
            throw new GuliException(20001,"注册信息不可为空");
        }
        //2.判断验证码是否正确
        String redisCode=redisTemplate.opsForValue().get(mobile);
        if(!code.equals(redisCode)){
            throw new GuliException(20001,"验证码错误");
        }
        //3.判断手机号是否已被注册
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("mobile",mobile);
        Integer integer = baseMapper.selectCount(queryWrapper);
        if(integer !=0){
            throw new GuliException(20001,"手机已被注册");
        }
        //4.插入到数据库，包括密码加密，禁用信息，用户默认头像
        Member member=new Member();
        member.setNickname(nickName);
        member.setMobile(mobile);
        member.setPassword(MD5.encrypt(password));
        member.setIsDisabled(false);
        member.setAvatar("https://online-teach-file.oss-cn-beijing.aliyuncs.com/teacher/2019/10/30/b8aa36a2-db50-4eca-a6e3-cc6e608355e0.png");
        baseMapper.insert(member);
    }

    @Override
    public Member getOpenIdMember(String openid) {
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("openid",openid);
        Member member = baseMapper.selectOne(queryWrapper);
        return member;
    }

    @Override
    public Integer countRegister(String day) {
        return baseMapper.countRegister(day);
    }
}
