package com.example.ucenter.controller;


import com.example.ucenter.entity.Member;
import com.example.ucenter.entity.vo.RegisterVo;
import com.example.ucenter.service.MemberService;
import com.example.utils.CommonResult;
import com.example.utils.JwtUtils;
import com.example.utils.ordervo.UcenterMemberOrder;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 会员表 前端控制器
 * </p>
 *
 * @author 李泽伟
 * @since 2022-05-31
 */
@RestController
@RequestMapping("/api/ucenter/member")
//@CrossOrigin
public class MemberController {

    @Resource
    MemberService memberService;
    //用户登录
    @PostMapping("login")
    public CommonResult login(@RequestBody Member member){
        String token=memberService.login(member);
        return CommonResult.ok().data("token",token);
    }

    //用户注册
    @PostMapping(value = "register")
    public CommonResult register(@RequestBody RegisterVo registerVo){
        memberService.register(registerVo);
        return CommonResult.ok();
    }

    //根据token获得用户信息
    @GetMapping(value = "getMemberInfo")
    public CommonResult getMemberInfo(HttpServletRequest request){
        String memberIdByJwtToken = JwtUtils.getMemberIdByJwtToken(request);
        Member member = memberService.getById(memberIdByJwtToken);
        return CommonResult.ok().data("userInfo",member);
    }

    //根据用户id获得用户信息
    @PostMapping(value = "getUserInfoOrder/{id}")
    public UcenterMemberOrder getUserInfoOrder(@PathVariable String id){
        Member byId = memberService.getById(id);
        UcenterMemberOrder ucenterMemberOrder=new UcenterMemberOrder();
        BeanUtils.copyProperties(byId,ucenterMemberOrder);
        return ucenterMemberOrder;
    }

    //查询某一天用户的注册数量
    @GetMapping(value = "countRegister/{day}")
    public CommonResult countRegister(@PathVariable String day){
        Integer count=memberService.countRegister(day);
        return CommonResult.ok().data("countRegister",count);
    }

}

