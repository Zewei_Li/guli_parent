package com.example.ucenter.mapper;

import com.example.ucenter.entity.Member;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会员表 Mapper 接口
 * </p>
 *
 * @author 李泽伟
 * @since 2022-05-31
 */
public interface MemberMapper extends BaseMapper<Member> {

    Integer countRegister(String day);
}
