package com.example.vod.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface VodService {
    String uploadVideoAly(MultipartFile file);

    void removeAlyVideo(String id);

    //删除课程-删除课程中所有视频
    void removeMoreAlyVideo(List ids);
}
