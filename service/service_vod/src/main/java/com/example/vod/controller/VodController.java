package com.example.vod.controller;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthRequest;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthResponse;
import com.example.base.handler.Exception.GuliException;
import com.example.utils.CommonResult;
import com.example.vod.service.VodService;
import com.example.vod.util.InitVodCilent;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/eduvod/video")
//@CrossOrigin
public class VodController {
    @Autowired
    private VodService vodService;

    //上传视频到阿里云
    @PostMapping("uploadAlyiVideo")
    public CommonResult uploadAlyiVideo(MultipartFile file) {
        //返回上传视频id
        String videoId = vodService.uploadVideoAly(file);
        return CommonResult.ok().data("videoId",videoId);
    }

    //删除阿里云上的视频
    @DeleteMapping(value = "removeAlyVideo/{id}")
    public CommonResult removeAlyVideo(@PathVariable String id){
        vodService.removeAlyVideo(id);
        return CommonResult.ok();
    }

    //删除课程-删除课程中所有视频
    @DeleteMapping(value = "delete-batch")
    public CommonResult deleteBatch(@RequestParam("ids") List<String> ids){
        vodService.removeMoreAlyVideo(ids);
        return CommonResult.ok();
    }

    //根据视频id获得播放凭证
    @GetMapping("getPlayAuth/{id}")
    public CommonResult getPlayAuth(@PathVariable String id){
        DefaultAcsClient client = null;
        try {
            client = InitVodCilent.initVodClient("LTAI5tBUYwEuz4tzRDrXHBxw", "cv0vhb6EOd9mQOhj44NzR1LpmA56Iw");
            GetVideoPlayAuthRequest request = new GetVideoPlayAuthRequest();
            request.setVideoId(id);

            GetVideoPlayAuthResponse response = client.getAcsResponse(request);
            System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++");
            System.out.println(response);
            return CommonResult.ok().data("playAuth",response.getPlayAuth());
        } catch (ClientException e) {
            e.printStackTrace();
            throw new GuliException(20001,"获取播放凭证失败");
        }
    }
}
